# Simple makefile for cpp project

Small makefile template and project structure for project

+ [cpp-cli](cpp-cli) Small template for a cpp cli based application
+ [latex-md](latex-md) convert all md and tex files to pdf

## Licence
[LGPL-3](LICENCE)
